
package Modul1;

public class MasyarakatSekitar extends Penduduk {

  private String nomor;
  
  public MasyarakatSekitar(){
  }
    public void setNomor(String nomor) {
        this.nomor = nomor;
    }

    public String getNomor() {
        return nomor;
    }
  @Override
  public double hitungIuran(){
        return  Integer.parseInt(nomor)* 100;
     
  }

    @Override
    public String getStatus() {
        return "Masyarakat Sekitar";
}}
