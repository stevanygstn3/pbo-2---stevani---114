package Modul1;

public class MainMahasiswa {

    static public UKM ukm = new UKM();
    static public Mahasiswa mahasiswa;
    static public MasyarakatSekitar msySekitar;
    static public Penduduk anggota[] = new Penduduk[4];
    static double bayar = 0;

    public static void main(String[] args) {
        ukm.setNamaUnit("Tears USD");

        Mahasiswa ketua = new Mahasiswa();
        ketua.setNim("185314114");
        ketua.setNama("Stevi");
        ketua.setTanggalLahir("31 Agustus 2000");
        ukm.setKetua(ketua);

        Mahasiswa sekretaris = new Mahasiswa();
        sekretaris.setNim("185314001");
        sekretaris.setNama("Zamora");
        sekretaris.setTanggalLahir("5 Januari 2000");
        ukm.setSekretaris(sekretaris);

        inputData();
        cetak();
    }

    public static void inputData() {

        MasyarakatSekitar msySekitar1 = new MasyarakatSekitar();
        msySekitar1.setNomor("1022");
        msySekitar1.setNama("Lilunanata");
        msySekitar1.setTanggalLahir("31 Desember 1993");

        MasyarakatSekitar msySekitar2 = new MasyarakatSekitar();
        msySekitar2.setNomor("2134");
        msySekitar2.setNama("Lily Ardian");
        msySekitar2.setTanggalLahir("30 Desember 1993");

        Mahasiswa mahasiswa1 = new Mahasiswa();
        mahasiswa1.setNim("185211113");
        mahasiswa1.setNama("Luvitania");
        mahasiswa1.setTanggalLahir("1 Januari 1997");

        Mahasiswa mahasiswa2 = new Mahasiswa();
        mahasiswa2.setNim("185211279");
        mahasiswa2.setNama("Amnestheticia");
        mahasiswa2.setTanggalLahir("1 Januari 1998");

        ukm.setAnggota(anggota);
        anggota[0] = new MasyarakatSekitar();
        anggota[0] = msySekitar1;
        anggota[1] = new Mahasiswa();
        anggota[1] = mahasiswa1;
        anggota[2] = new MasyarakatSekitar();
        anggota[2] = msySekitar2;
        anggota[3] = new Mahasiswa();
        anggota[3] = mahasiswa2;

        System.out.println("Nama UKM\t: " + ukm.getNamaUnit());
        System.out.println("");
        System.out.println("Ketua\t\t: " + ukm.getKetua().nama);
        System.out.println("NIM \t\t: " + ukm.getKetua().getNim());
        System.out.println("Tanggal Lahir\t: " + ukm.getKetua().tanggalLahir);
        System.out.println("");
        System.out.println("Sekretaris\t: " + ukm.getSekretaris().nama);
        System.out.println("NIM \t\t: " + ukm.getSekretaris().getNim());
        System.out.println("Tanggal Lahir\t: " + ukm.getSekretaris().tanggalLahir);
        System.out.println("");

    }

    public static void cetak() {
        System.out.println("Data Anggota : ");
        System.out.println();

        System.out.print("===========================================================");
        System.out.println("=========================================================");
        System.out.printf("%-8s",  ("No"));
        System.out.printf("%-25s", ("Nama"));
        System.out.printf("%-20s", ("Tanggal lahir"));
        System.out.printf("%-25s", ("Status"));
        System.out.printf("%-25s", ("Iuran"));
        System.out.println();
        System.out.print("===========================================================");
        System.out.println("==========================================================");

        for (int i = 0; i < anggota.length; i++) {
            bayar += anggota[i].hitungIuran();
            System.out.printf("%-8s", (i + 1));
            System.out.printf("%-25s", (ukm.getAnggota()[i].getNama()));
            System.out.printf("%-20s", (ukm.getAnggota()[i].getTanggalLahir()));
            System.out.printf("%-25s", (ukm.getAnggota()[i].getStatus()));
            System.out.printf("%-25s", (ukm.getAnggota()[i].hitungIuran()));
            System.out.println();
        }
        System.out.print("====================================================");
        System.out.println("====================================================");
        System.out.printf("%-78s", "Total Keseluruhan Iuran ");
        System.out.printf("%-20s", +bayar);
    }

}
