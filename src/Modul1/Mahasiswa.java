package Modul1;

public class Mahasiswa extends Penduduk {
private String nim;
    

    public Mahasiswa() {

    }

    public void setNim(String nim) {
        this.nim = nim;
    }

    public String getNim() {
        return nim;
    }

    @Override
    public double hitungIuran() {
        return Integer.parseInt(nim) / 1000;

    }

    @Override
    public String getStatus() {
        return "Mahasiswa";
    }
   

}
