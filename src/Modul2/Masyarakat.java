package Modul2;

public class Masyarakat extends Penduduk implements Peserta{

    private String nomor;

    public Masyarakat() {
    }

    public void setNomor(String nomor) {
        this.nomor = nomor;
    }

    public String getNomor() {
        return nomor;
    }

    @Override
    public double hitungIuran() {
        return Integer.parseInt(nomor) * 100;

    }

    @Override
    public String getStatus() {
        return "Masyarakat ";
    }
    
    @Override
    public String getJenisSertifikat(){
        return "Peserta";
    }
    @Override
    public String getFasilitas(){
        return "block note, alat tulis dan modul pelatihan";
    }
    @Override
    public String getKonsumsi(){
        return "snack dan makan siang";
    }
    
    
}
