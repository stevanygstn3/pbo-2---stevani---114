
package Modul2;

public class Mahasiswa extends Penduduk implements Peserta {
private String nim;
    

    public Mahasiswa() {

    }

    public void setNim(String nim) {
        this.nim = nim;
    }

    public String getNim() {
        return nim;
    }

    @Override
    public double hitungIuran() {
        return Integer.parseInt(nim) / 10000;

    }

    @Override
    public String getStatus() {
        return "Mahasiswa";
    }
    
@Override
    public String getJenisSertifikat(){
        return "Panitia";
    }
@Override
    public String getFasilitas(){
        return "block note , alat tulis, laptop";
    }
@Override
    public String getKonsumsi(){
        return "snack, makan siang, makan malam";
    }
   

}
