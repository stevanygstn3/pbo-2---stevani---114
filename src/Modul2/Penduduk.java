
package Modul2;

import java.util.Scanner;

public abstract class Penduduk {

    Scanner sc = new Scanner(System.in);
    protected String nama, tanggalLahir;
    private Mahasiswa mhs;
    private Masyarakat msy;

    public Penduduk() {

    }

    public Penduduk(String nama, String tanggalLahir) {
        this.nama = nama;
        this.tanggalLahir = tanggalLahir;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public void setTanggalLahir(String tanggalLahir) {
        this.tanggalLahir = tanggalLahir;
    }

    public String getNama() {
        return nama;
    }

    public String getTanggalLahir() {
        return tanggalLahir;
    }

    public Mahasiswa getMhs() {
        return mhs;
    }

    public void setMhs(Mahasiswa mhs) {
        this.mhs = mhs;
    }

    public Masyarakat getMsy() {
        return msy;
    }

    public void setMsy(Masyarakat msy) {
        this.msy = msy;
    }
    
    

    public abstract double hitungIuran();

    public abstract String getStatus();
}

