package Modul2;

public class Main {

    static public UKM ukm = new UKM();
    static public Mahasiswa mahasiswa;
    static public Masyarakat masyarakat;
    static public Peserta peserta;
    static public Penduduk anggota[] = new Penduduk[3];
    static double bayar = 0;

    public static void main(String[] args) {
        ukm.setNamaUnit("Pelatihan Internet");

        Mahasiswa ketua = new Mahasiswa();
        ketua.setNim("185314114");
        ketua.setNama("Edo");
        ketua.setTanggalLahir("30 Agustus 2000");
        ukm.setKetua(ketua);

        Mahasiswa sekretaris = new Mahasiswa();
        sekretaris.setNim("185314001");
        sekretaris.setNama("Atma");
        sekretaris.setTanggalLahir("5 Januari 2000");
        ukm.setSekretaris(sekretaris);

        inputData();
        cetak();
    }

    public static void inputData() {
        Mahasiswa mahasiswa1 = new Mahasiswa();
        mahasiswa1.setNim("185314100");
        mahasiswa1.setNama("Aji");
        mahasiswa1.setTanggalLahir("1 Januari 1997");
//        mahasiswa1.getJenisSertifikat();
//        mahasiswa1.getFasilitas();
//        mahasiswa1.getKonsumsi();

        Mahasiswa mahasiswa2 = new Mahasiswa();
        mahasiswa2.setNim("185314101");
        mahasiswa2.setNama("Bayu");
        mahasiswa2.setTanggalLahir("1 Januari 1998");
//        mahasiswa2.getJenisSertifikat();
//        mahasiswa2.getFasilitas();
//        mahasiswa2.getKonsumsi();

        Masyarakat masyarakat1 = new Masyarakat();
        masyarakat1.setNomor("12531");
        masyarakat1.setNama("Vandi");
        masyarakat1.setTanggalLahir("31 Desember 1993");
//        masyarakat1.getJenisSertifikat();
//        masyarakat1.getFasilitas();
//        masyarakat1.getKonsumsi();

        ukm.setAnggota(anggota);

        anggota[0] = new Mahasiswa();
        anggota[0] = mahasiswa1;
        anggota[1] = new Mahasiswa();
        anggota[1] = mahasiswa2;
        anggota[2] = new Masyarakat();
        anggota[2] = masyarakat1;

        System.out.println("Nama UKM\t: " + ukm.getNamaUnit());
        System.out.println("Ketua\t\t: " + ukm.getKetua().nama);
//      System.out.println("NIM \t\t: " + ukm.getKetua().getNim());
        System.out.println("Sekretaris\t: " + ukm.getSekretaris().nama);
//      System.out.println("NIM \t\t: " + ukm.getSekretaris().getNim());
//      System.out.println("Tanggal Lahir\t: " + ukm.getSekretaris().tanggalLahir);
        System.out.println("");

    }

    public static void cetak() {
        System.out.println("Data Anggota : ");
        System.out.println("");
        System.out.print("================================================================================");
        System.out.println("==============================================================================");
        System.out.printf("%-8s", ("No"));
        System.out.printf("%-15s", ("Nama"));
//      System.out.printf("%-20s", ("Tanggal lahir"));
        System.out.printf("%-15s", ("Iuran"));
        System.out.printf("%-15s", ("Status"));
        System.out.printf("%-20s", ("Sertifikat"));
        System.out.printf("%-50s", ("Fasilitas"));
        System.out.printf("%-25s", ("Konsumsi"));
        System.out.println();
        System.out.print("===============================================================================");
        System.out.println("==============================================================================");

        for (int i = 0; i < anggota.length; i++) {
            bayar += anggota[i].hitungIuran();
            System.out.printf("%-8s", (i + 1));
            System.out.printf("%-15s", (ukm.getAnggota()[i].getNama()));
//          System.out.printf("%-20s", (ukm.getAnggota()[i].getTanggalLahir()));
            System.out.printf("%-15s", (ukm.getAnggota()[i].hitungIuran()));
            System.out.printf("%-15s", (ukm.getAnggota()[i].getStatus()));

            if (ukm.getAnggota()[i] instanceof Mahasiswa) {
                Mahasiswa mah = (Mahasiswa) anggota[i];
                System.out.printf("%-20s", (mah.getJenisSertifikat()));
                System.out.printf("%-50s", (mah.getFasilitas()));
                System.out.printf("%-25s", (mah.getKonsumsi()));

            } else if (ukm.getAnggota()[i] instanceof Masyarakat) {
                Masyarakat mas = (Masyarakat) anggota[i];
                System.out.printf("%-20s", (mas.getJenisSertifikat()));
                System.out.printf("%-50s", (mas.getFasilitas()));
                System.out.printf("%-25s", (mas.getKonsumsi()));

            }

            System.out.println();
        }
        System.out.print("===================================================================================");
        System.out.println("=================================================================================");
        System.out.printf("%-23s", "Total Iuran ");
        System.out.printf("%-20s", +bayar);
    }

}
