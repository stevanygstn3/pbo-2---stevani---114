
package GUI;
import javax.swing.JFrame;


    public class JFrameClass2 extends JFrame {
    private static final int FRAME_WIDTH    = 300;
    private static final int FRAME_HEIGHT   = 200;
    private static final int FRAME_X_ORIGIN = 150;
    private static final int FRAME_Y_ORIGIN = 250;

    public JFrameClass2( ) {
        //set the frame default properties
        setTitle     ( "PBO II" );
        setSize      ( FRAME_WIDTH, FRAME_HEIGHT  );
        setLocation  ( FRAME_X_ORIGIN, FRAME_Y_ORIGIN );
        setResizable(false);
        setDefaultCloseOperation( EXIT_ON_CLOSE );

   }

}   

