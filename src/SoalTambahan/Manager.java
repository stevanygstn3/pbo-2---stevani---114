
package SoalTambahan;

/**
 *
 * @author LENOVO
 */
public class Manager extends Pegawai implements TugasBelajar {
    private int jumAnak, istri, jamKerja;
    private String study;
    
    public Manager (int jumAnak, int istri, int jamKerja, String study, String nip, String nama, int golongan){
        super(nip, nama, golongan);
        this.jumAnak = jumAnak;
        this.istri = istri;
        this.jamKerja = jamKerja;
        this.study = study;
    }

    public int getJumAnak() {
        return jumAnak;
    }

    public void setJumAnak(int jumAnak) {
        this.jumAnak = jumAnak;
    }

    public int getIstri() {
        return istri;
    }

    public void setIstri(int istri) {
        this.istri = istri;
    }

    public int getJamKerja() {
        return jamKerja;
    }

    public void setJamKerja(int jamKerja) {
        this.jamKerja = jamKerja;
    }

    public String getStudy() {
        return study;
    }

    public void setStudy(String study) {
        this.study = study;
    }
    public double hitTunjangan (){
        if (jumAnak < 3 && istri <3 ){
            return (jumAnak *100000) + (istri *200000);
        }
        else {
            return (3 *100000)+(3*200000);
        }
    }
    public double hitLembur(){
        if (jamKerja > 8){
            return (jamKerja - 8)*50000;
        }else {
            return 0;
        }
    }
    @Override
    public double hitungGatot(){
        if (isSelesai()== true) {
            gajiTotal = hitGajiPokok(golongan) + 500000;
            
        } else {
            gajiTotal = hitGajiPokok(golongan) + hitLembur() + hitTunjangan();
        }
        return gajiTotal;
    }
    public boolean isSelesai(){
        boolean status = false;
        if (getStudy().equalsIgnoreCase("Studi")) {
            status = true;
        } else {
            status = false;
        }
        return status;

    }
    }

    
    

