package SoalTambahan;

/**
 *
 * @author LENOVO
 */
public class Main {

    public static double gajiTotal;

    public static void main(String[] args) {
        System.out.println("DATA PEGAWAI");
        Pegawai peg[] = new Pegawai[2];
        Manager man = new Manager(2, 10, 1, "Selesai", "P.123", "Budi", 3);
        Kantor kantor = new Kantor(man, peg);
        Marketing mar = new Marketing(12, "Studi", "P.234", "Agus", 1);
        peg[0] = mar;
        Honorer hon = new Honorer("P.987", "Beni", 2);
        peg[1] = hon;

        System.out.println("USD");
        System.out.println("Data Manager");
        System.out.println("Nama\t\t :" + man.nama);
        System.out.println("Nip\t\t :" + man.nip);
        System.out.println("Status\t :" + man.getStudy());
        System.out.println("Gaji Total\t :" + man.hitungGatot());
        System.out.println("");

        System.out.println("DATA PEGAWAI");
        System.out.println("");
        for (int i = 0; i < peg.length; i++) {
            System.out.println("");
            System.out.println("Nama\t\t :" + peg[i].nama);
            System.out.println("Nip\t\t :" + peg[i].nip);
            System.out.println("Gaji Total\t :" + peg[i].hitungGatot());

            if (peg[i] instanceof Marketing) {
                Marketing marketing = (Marketing) peg[i];
                System.out.println("Status\t :" + marketing.getStudy());
                System.out.println("");
            } else if (peg[i] instanceof Honorer) {
                Honorer honorer = (Honorer) peg[i];
                System.out.println("Status\t :-");
                System.out.println("");

            }
            gajiTotal = kantor.hitGajiPeg();

        }
        System.out.println("Total gaji\t : " + gajiTotal);
    }
}
