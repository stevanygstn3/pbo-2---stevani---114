package SoalTambahan;

/**
 *
 * @author LENOVO
 */
public class Marketing extends Pegawai implements TugasBelajar {

    private int jamKerja;
    private String study;

    public Marketing(int jamKerja, String study, String nip, String nama, int golongan) {
        super(nip, nama, golongan);
        this.jamKerja = jamKerja;
        this.study = study;
    }

    public int getJamKerja() {
        if (jamKerja > 8) {
            jamKerja = jamKerja * 50000;

        }
        return jamKerja;
    }

    public void setJamKerja(int jamKerja) {
        this.jamKerja = jamKerja;
    }

    public String getStudy() {
        return study;
    }

    public void setStudy(String study) {
        this.study = study;
    }

    public double hitLembur() {
        if (jamKerja > 8) {
            return (jamKerja - 8) * 50000;
        } else {
            return 0;
        }
    }

    public double hitungGatot() {
        if (isSelesai() == true) {
            gajiTotal = hitGajiPokok(golongan) + 300000;
        } else {
            gajiTotal = hitGajiPokok(golongan) + hitLembur();
        }
        return gajiTotal;

    }

    public boolean isSelesai() {
        boolean status = false;
        if (getStudy().equalsIgnoreCase("Studi")) {
            status = true;
        } else {
            status = false;
        }
        return status;

    }
}
