
package SoalTambahan;

/**
 *
 * @author LENOVO
 */
public class Kantor {
    Manager man;
    Pegawai peg[];
    
    public Kantor( Manager man,  Pegawai[] peg){
        this.man = man;
        this.peg = peg;
    }

    public Manager getMan() {
        return man;
    }

    public void setMan(Manager man) {
        this.man = man;
    }

    public Pegawai[] getPeg() {
        return peg;
    }

    public void setPeg(Pegawai[] peg) {
        this.peg = peg;
    }
    public double hitGajiPeg(){
        double gaji = 0;
        for (int i = 0; i < peg.length; i++) {
            gaji += peg[i].hitungGatot();
            
        }
        return gaji;
    }
    
}
